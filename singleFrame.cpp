#include "stdafx.h"
#include<iostream>
#include <opencv2/core.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <opencv2/videoio.hpp>
#include "opencv2/opencv.hpp"
#include "opencv2/core/cuda.hpp"
using namespace cv;
using namespace std;
Scalar defineColor(Mat m) {
	Mat hsv, mask;
	cv::cvtColor(m, hsv, COLOR_BGR2HSV);
	Scalar green_light_l(35,35,40);
	Scalar green_light_h(46, 95, 185);
	

	//calcHist(m, 1, channels, mask, hist_base, 3, histSize, ranges, true, false); //mask is the mask of the objec


	inRange(hsv, Scalar(15, 35, 80), Scalar(26, 255, 255), mask);
	double orangeSum = cv::sum(mask)[0] / (mask.rows*mask.cols);
	Scalar maxColor = Scalar(0, 165, 255);
	double maxSum = orangeSum;
	cout << "Orange sum " << orangeSum << endl;
	inRange(hsv, green_light_l, green_light_h, mask);
	double greenSum = cv::sum(mask)[0] / (mask.rows*mask.cols);
	cout << "Green sum " << greenSum << endl;
	if (greenSum > maxSum)
	{
		maxSum = greenSum;
		maxColor = Scalar(0, 255, 128);
	}
	cout << "sum " << orangeSum <<endl;
	imshow("mask", mask);
	if(maxSum>1.0)
		return maxColor;		
	else 
		return Scalar(255, 255, 255);//white
}
