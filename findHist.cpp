#include "stdafx.h"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <opencv2/videoio.hpp>
#include "opencv2/opencv.hpp"
#include "opencv2/core/cuda.hpp"
using namespace cv;
using namespace std;
Scalar colorByHist(Mat m) {

	Mat hsv;
	
	cvtColor(m, hsv, COLOR_BGR2HSV);
	int hbins = 7, sbins = 32;
	int histSize[] = { hbins };
	float hranges[] = { 0, 180 };
	const float* ranges[] = { hranges };
	MatND hist;
	int channels[] = { 0};

	calcHist(&hsv, 1, channels, Mat(), // do not use mask
		hist, 1, histSize, ranges,
		true, // the histogram is uniform
		false);
	double maxVal = 0;
	/*
	cout << "HIST: ";
	for (int i = 0; i < hbins; i++)
		cout << hist.at<float>(i) << " ";
	cout << endl;
	*/
	normalize(hist, hist, 1, 0, NORM_L2, -1, Mat());
	cout << " Norm Hist: ";
	for (int i = 0; i < hbins; i++)
		cout << hist.at<float>(i) << " ";
	cout << endl;
	Scalar color = Scalar(255, 255, 255);
	if(hist.at<float>(0)>0.8) // 1-st bucket - Orange
		return Scalar(0, 165, 255);
	if (hist.at<float>(1)>0.4) // 2-st bucket - light green
		return Scalar(0, 255, 128);

	return Scalar(255, 255, 255);//white
}