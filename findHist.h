#pragma once
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <opencv2/videoio.hpp>
#include "opencv2/opencv.hpp"
#include "opencv2/core/cuda.hpp"
using namespace cv;
using namespace std;
Scalar colorByHist(Mat m);